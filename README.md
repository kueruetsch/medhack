# Medhack Backend

Серверная часть проекта Medhack

Необходимые библиотеки
-------------

- Python >= 3.5
- остальные библиотеки находятся в файле requirements.txt 

Установка
-----

- Example Dependencies Setup

    - Install packages
    
            sudo apt-get update
            sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib nginx
            
    - Create the PostgreSQL Database and User
    
            sudo -u postgres psql
            
            # inside PostgreSQL prompt
            
            CREATE DATABASE medhack;
            CREATE USER medhackuser WITH PASSWORD 'PASSWORD' <----;
            
            ALTER ROLE medhackuser SET client_encoding TO 'utf8';
            ALTER ROLE medhackuser SET default_transaction_isolation TO 'read committed';
            ALTER ROLE medhackuser SET timezone TO 'UTC';
            
            GRANT ALL PRIVILEGES ON DATABASE medhack TO medhackuser;
    
    - Create a Python Virtual Environment 
    
            sudo -H pip3 install --upgrade pip
            sudo -H pip3 install virtualenv
            
            mkdir ~/pythonVenvs
            cd ~/pythonVenvs
            virtualenv medhack
            source medhack/bin/activate

    - Django Project Setup
    
            cd ~
            git clone https://github.com/dadaday/medhack.git
            cd medhack
            pip install -r requirements.txt
            
            touch settings.ini
            # fill out the settings
            
            python manage makemigrations
            python manage migrate
            python manage createsuperuser
            python manage collectstatic
            
            
            # to test the installation
            python manage runserver
    
    - Configuring Gunicorn and NGINX
            
            pip install gunicorn
            
            sudo nano /etc/systemd/system/medhack_gunicorn.service
            
            #use the GUNICORN example file, make sure to put the correct user
            
            sudo systemctl start medhack_gunicorn
            sudo systemctl enable medhack_gunicorn 
            sudo systemctl status medhack_gunicorn
            
            
            sudo nano /etc/nginx/sites-available/medhack
            sudo ln -s /etc/nginx/sites-available/medhack /etc/nginx/sites-enabled
            sudo nginx -t
            sudo systemctl restart nginx
            

Config Files examples:
-----------


- setting.ini
    
    
        [settings]
        DEBUG=
        SECRET_KEY=
        
        TIME_ZONE=Asia/Bishkek
        LANGUAGE_CODE=ru
        
        DB_ENGINE=django.db.backends.postgresql_psycopg2
        DB_NAME=
        DB_USER=
        DB_PASSWORD=
        DB_HOST=
        
        ALLOWED_HOSTS=127.0.0.1



- GUNICORN /etc/systemd/system/medhack_gunicorn.service


        [Unit]
        Description=gunicorn daemon
        After=network.target
        
        [Service]
        User=user
        Group=www-data
        WorkingDirectory=/home/user/medhack/project_folders
        ExecStart=/home/user/pythonVenvs/medhack/bin/gunicorn --access-logfile - --workers 3 --bind unix:/home/user/medhack/project_folders/medhack.sock config.wsgi:application
        
        [Install]
        WantedBy=multi-user.target
    

- NGINX

    
        server {
            listen 80;
            server_name server_domain_or_IP;
            
            client_max_body_size 10M;
        
            location = /favicon.ico { access_log off; log_not_found off; }
            location /static/ {
                root /home/user/medhack/project_folders;
            }
        
            location / {
                include proxy_params;
                proxy_pass http://unix:/home/user/medhack/project_folders/medhack.sock;
            }
        }  