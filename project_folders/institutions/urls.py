from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (
    InstitutionViewSet,
    MovementListView,
    RegionListView,
)

router = DefaultRouter()

router.register('institutions', InstitutionViewSet, base_name='institutions')

urlpatterns = [
    path('regions',
         view=RegionListView.as_view(),
         name='regions'
         ),
    path('institutions/<int:id>/movements',
         view=MovementListView.as_view(),
         name='movements'
         ),
    path('', include(router.urls)),
]
