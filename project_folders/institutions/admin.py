from django.contrib import admin
from django.contrib.admin import AdminSite
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _

from .models import (
    Institution, Region, UserModel, StockAdmin, Doctor
)
from .views import upload


# https://gist.github.com/rchrd2/c1cabcbfbfb27295a22c
class MyAdminSite(AdminSite):
    def get_urls(self):
        from django.conf.urls import url
        urls = super(MyAdminSite, self).get_urls()
        urls = [
                   url(r'^institution-upload/$', self.admin_view(upload))
               ] + urls
        return urls

    def each_context(self, request):
        if not request.user.is_anonymous:
            if request.user.is_superuser:
                self.site_header = _('Med Hack')
            else:
                stock_admin = StockAdmin.objects.filter(user=request.user)
                if stock_admin.exists():
                    stock_admin = stock_admin.first()
                    institution = stock_admin.institution
                    self.site_header = institution.short_name
        return super().each_context(request)


admin_site = MyAdminSite(name='myadmin')


class InstitutionAdmin(admin.ModelAdmin):
    list_display = ('short_name', 'code', 'region')
    list_filter = ('region',)
    search_fields = ('short_name', 'full_name', 'code')


class RegionAdmin(admin.ModelAdmin):
    pass


class StockAdminAdmin(admin.ModelAdmin):
    list_display = ('user', 'institution')
    raw_id_fields = ('institution',)


class DoctorAdmin(admin.ModelAdmin):
    list_display = ('name', 'institution', 'job')
    list_filter = ('institution',)

    def get_queryset(self, request):
        qs = super(DoctorAdmin, self).get_queryset(request)
        user = request.user
        if user.is_superuser:
            return qs

        # return available drugs related to user's institution
        stock_admin = StockAdmin.objects.filter(user=user)
        if stock_admin.exists():
            stock_admin = stock_admin.first()
            qs = qs.filter(institution=stock_admin.institution)
            return qs

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'institution':
            user = request.user
            if not user.is_superuser:
                stock_admin = StockAdmin.objects.filter(user=user)

                if stock_admin.exists():
                    stock_admin = stock_admin.first()
                    kwargs['initial'] = stock_admin.institution.id
                    kwargs['disabled'] = True

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin_site.register(UserModel, UserAdmin)
admin_site.register(Group, GroupAdmin)
admin_site.register(Institution, InstitutionAdmin)
admin_site.register(Region, RegionAdmin)
admin_site.register(StockAdmin, StockAdminAdmin)
admin_site.register(Doctor, DoctorAdmin)
