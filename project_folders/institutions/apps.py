from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class InstitutionsConfig(AppConfig):
    name = 'institutions'
    verbose_name = _('institutions')