from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

UserModel = get_user_model()


class Region(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("Region name"))

    class Meta:
        verbose_name = _("Region")
        verbose_name_plural = _("Regions")

    def __str__(self):
        return self.name


class Institution(models.Model):
    region = models.ForeignKey(
        Region, on_delete=models.CASCADE, null=True, blank=True,
        verbose_name=_('region')
    )

    code = models.CharField(max_length=20, verbose_name=_("organization code"))
    short_name = models.CharField(max_length=75, verbose_name=_("short name"))
    full_name = models.CharField(max_length=225, verbose_name=_("full name"))
    address = models.CharField(max_length=100, verbose_name=_("address"))

    class Meta:
        verbose_name = _("Institution")
        verbose_name_plural = _("Institutions")

    def __str__(self):
        return self.short_name


class StockAdmin(models.Model):
    user = models.OneToOneField(
        UserModel, on_delete=models.CASCADE,
        verbose_name=_('User')
    )
    institution = models.ForeignKey(
        Institution,
        on_delete=models.PROTECT,
        verbose_name=_('Institution')
    )

    class Meta:
        verbose_name = _('Stock admin')
        verbose_name_plural = _('Stock admins')

    def __str__(self):
        return self.user.get_full_name()


class Doctor(models.Model):
    institution = models.ForeignKey(
        Institution,
        on_delete=models.PROTECT,
        verbose_name=_('Institution')
    )

    name = models.CharField(max_length=100, verbose_name=_("fio"))
    job = models.CharField(max_length=100, verbose_name=_("job"))

    class Meta:
        verbose_name = _('Doctor')
        verbose_name_plural = _('Doctors')

    def __str__(self):
        return self.name
