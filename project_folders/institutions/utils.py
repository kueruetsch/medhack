from django.db import transaction
from openpyxl import load_workbook
from rest_framework.pagination import PageNumberPagination

from institutions.models import Region, Institution


class CustomPagination(PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 100


def parseAndStoreExcel(filename):
    wb = load_workbook(filename)
    sheets = wb.sheetnames
    print(sheets)

    institutions = []

    with transaction.atomic():
        Institution.objects.all().delete()

        for sheet_name in sheets:
            region, created = Region.objects.get_or_create(name=sheet_name)

            worksheet = wb[sheet_name]

            for row in worksheet.iter_rows(min_row=2, min_col=1):
                code = str(row[0].value)
                short_name = str(row[1].value)
                full_name = str(row[2].value)
                institutions.append(Institution(code=code, short_name=short_name,
                                                full_name=full_name, region=region))

    Institution.objects.bulk_create(institutions)

    return True, "screw you"
