from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils.translation import ugettext_lazy as _
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet

from drugs.models import DrugMovement
from drugs.serializers import DrugMovementSerializer
from institutions.forms import UploadFileForm
from .models import Institution, Region
from .serializers import (
    InstitutionSerializer, InstitutionDetailedSerializer,
    RegionSerializer
)
from .utils import parseAndStoreExcel


class RegionListView(ListAPIView):
    queryset = Region.objects.all().order_by('name')
    serializer_class = RegionSerializer


class MovementListView(ListAPIView):
    serializer_class = DrugMovementSerializer

    def get_queryset(self):
        institution_id = self.kwargs['id']
        qs = DrugMovement.objects.filter(
            doctor__institution__pk=institution_id).order_by("-date")
        return qs


class InstitutionViewSet(ReadOnlyModelViewSet):
    queryset = Institution.objects.all().order_by('short_name')
    serializer_class = InstitutionSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('code', 'short_name', 'full_name')
    filter_fields = ('region',)

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = InstitutionDetailedSerializer
        return super().retrieve(request, *args, **kwargs)


@login_required(login_url='/admin/login/')
def upload(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            excel_file = request.FILES['file']
            successful, message = parseAndStoreExcel(filename=excel_file)
            if successful:
                messages.add_message(request, messages.SUCCESS, message)
            else:
                messages.add_message(request, messages.ERROR, message)
            redirect('admin:index')
    else:
        form = UploadFileForm()
    return render(
        request,
        'excel_parser/upload_form.html',
        {
            'form': form,
            'title': _('Upload Excel file with institutions list'),
            'header': _('Please upload the suitable Excel file')
        })
