from rest_framework import serializers

from drugs.serializers import InstitutionStockSerializer
from institutions.models import Institution, Region, Doctor


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'


class InstitutionSerializer(serializers.ModelSerializer):
    region = serializers.StringRelatedField()

    class Meta:
        model = Institution
        fields = '__all__'


class InstitutionDetailedSerializer(serializers.ModelSerializer):
    region = serializers.StringRelatedField()
    available_drugs = InstitutionStockSerializer(many=True)

    class Meta:
        model = Institution
        fields = '__all__'


class DoctorSerializer(serializers.ModelSerializer):
    institution = serializers.StringRelatedField()

    class Meta:
        model = Doctor
        fields = '__all__'
