from django.urls import path

from .views import (
    DrugMovementListView,
    DrugListView)

urlpatterns = [
    path('drugs',
         view=DrugListView.as_view(),
         name='drugs'
         ),
    path('movements',
         view=DrugMovementListView.as_view(),
         name='movements'
         ),
]
