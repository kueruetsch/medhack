from django.utils.translation import gettext_lazy as _

PIECE = 0
AMPULE = 1
PILL = 2
GAS = 3
DROPS = 4

STORAGE_TYPES = (
    (PIECE, _('Piece')),
    (AMPULE, _('Ampule')),
    (PILL, _('Pill')),
    (GAS, _('Gas')),
    (DROPS, _('Drops')),
)
