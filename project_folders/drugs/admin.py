from django.contrib import admin

from institutions.admin import admin_site
from institutions.models import StockAdmin, Doctor
from .models import Drug, DrugMovement, Stock


class DrugMovementInline(admin.TabularInline):
    model = DrugMovement
    readonly_fields = ('doctor', 'date', 'count')


class DrugAdmin(admin.ModelAdmin):
    list_display = ('com_name', 'trade_name', 'code', 'storage')
    list_filter = ('storage',)


class DrugMovementAdmin(admin.ModelAdmin):
    list_display = ('stock', 'doctor', 'institution', 'date', 'count')

    @staticmethod
    def institution(obj):
        return obj.doctor.institution.short_name

    def get_queryset(self, request):
        qs = super(DrugMovementAdmin, self).get_queryset(request)
        user = request.user
        if user.is_superuser:
            return qs

        # return available drugs related to user's institution
        stock_admin = StockAdmin.objects.filter(user=user)
        if stock_admin.exists():
            stock_admin = stock_admin.first()
            qs = qs.filter(doctor__institution=stock_admin.institution)
            return qs

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # drop down list with doctors related to user's institution
        if db_field.name == 'doctor':
            user = request.user
            if not user.is_superuser:
                stock_admin = StockAdmin.objects.filter(user=user)

                if stock_admin.exists():
                    stock_admin = stock_admin.first()
                    doctors = Doctor.objects.filter(
                        institution=stock_admin.institution)
                    kwargs['queryset'] = doctors

        if db_field.name == 'stock':
            user = request.user
            if not user.is_superuser:
                stock_admin = StockAdmin.objects.filter(user=user)

                if stock_admin.exists():
                    stock_admin = stock_admin.first()
                    available_drugs = Stock.objects.filter(
                        institution=stock_admin.institution).filter(count__gt=0)
                    kwargs['queryset'] = available_drugs

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class StockModelAdmin(admin.ModelAdmin):
    list_display = ('drug', 'institution', 'count', 'date')
    search_fields = ('drug', 'institution')
    inlines = (DrugMovementInline,)

    def get_queryset(self, request):
        qs = super(StockModelAdmin, self).get_queryset(request)
        user = request.user
        if user.is_superuser:
            return qs

        # return available drugs related to user's institution
        stock_admin = StockAdmin.objects.filter(user=user)
        if stock_admin.exists():
            stock_admin = stock_admin.first()
            qs = qs.filter(institution=stock_admin.institution)
            return qs

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'institution':
            user = request.user
            if not user.is_superuser:
                stock_admin = StockAdmin.objects.filter(user=user)

                if stock_admin.exists():
                    stock_admin = stock_admin.first()
                    kwargs['initial'] = stock_admin.institution.id
                    kwargs['disabled'] = True

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin_site.register(Drug, DrugAdmin)
admin_site.register(DrugMovement, DrugMovementAdmin)
admin_site.register(Stock, StockModelAdmin)
