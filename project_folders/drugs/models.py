from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import F
from django.utils.translation import ugettext_lazy as _

from institutions.models import Institution, Doctor
from . import choices


class Drug(models.Model):
    com_name = models.CharField(max_length=200,
                                verbose_name=_("Commercial name"))
    trade_name = models.CharField(max_length=200, verbose_name=_("Trade name"))
    code = models.CharField(max_length=100, verbose_name=_("Code"))
    storage = models.IntegerField(choices=choices.STORAGE_TYPES,
                                  verbose_name=_("Storage type"))

    class Meta:
        verbose_name = _("Drug")
        verbose_name_plural = _("Drugs")

    def __str__(self):
        return self.com_name


class Stock(models.Model):
    drug = models.ForeignKey(
        Drug,
        on_delete=models.PROTECT,
        verbose_name=_('drug'),
        related_name='available_drugs'
    )
    institution = models.ForeignKey(
        Institution,
        on_delete=models.PROTECT,
        verbose_name=_('Institution'),
        related_name='available_drugs'
    )
    count = models.PositiveIntegerField(verbose_name=_("count"))
    date = models.DateField(verbose_name=_('Date added'))

    class Meta:
        verbose_name = _("Available drug")
        verbose_name_plural = _("Available drugs")

    def __str__(self):
        return "{} - {} - {}".format(self.drug, self.institution, self.count)


class DrugMovement(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.PROTECT,
                              verbose_name=_('stock'), related_name='movements')
    doctor = models.ForeignKey(Doctor, on_delete=models.PROTECT,
                               verbose_name=_('doctor'),
                               related_name='movements')

    date = models.DateField(verbose_name=_("movement date"))
    count = models.PositiveIntegerField(verbose_name=_("count"))

    class Meta:
        verbose_name = _("Drug movement")
        verbose_name_plural = _("Drug movements")

    def clean(self):
        super().clean()
        if not self.stock.institution == self.doctor.institution:
            raise ValidationError(
                _('Stock and doctor are from different institutions'))

        if self.stock.count < self.count:
            raise ValidationError(_('Not enough available drugs'))

    def save(self, *args, **kwargs):
        self.stock.count = F('count') - self.count
        self.stock.save()
        super().save(*args, **kwargs)

    def __str__(self):
        return "{} {}: {}, {}".format(self.count, self.stock.drug, self.doctor,
                                      self.date)
