from rest_framework import serializers

from . import choices
from .models import DrugMovement, Drug, Stock


class DrugSerializer(serializers.ModelSerializer):
    storage = serializers.SerializerMethodField()

    @staticmethod
    def get_storage(obj):
        return choices.STORAGE_TYPES[obj.storage][1]

    class Meta:
        model = Drug
        fields = '__all__'


class DrugMovementSerializer(serializers.ModelSerializer):
    drug = serializers.SerializerMethodField()
    code = serializers.SerializerMethodField()
    doctor = serializers.StringRelatedField()

    def get_drug(self, obj):
        return obj.stock.drug.com_name

    def get_code(self, obj):
        return obj.stock.drug.code

    class Meta:
        model = DrugMovement
        exclude = ('stock', 'id')


class InstitutionStockSerializer(serializers.ModelSerializer):
    drug = DrugSerializer()

    class Meta:
        model = Stock
        exclude = ('id', 'institution')
