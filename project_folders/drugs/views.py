from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView

from .models import Drug, DrugMovement
from .serializers import DrugMovementSerializer, DrugSerializer


class DrugListView(ListAPIView):
    queryset = Drug.objects.all()
    serializer_class = DrugSerializer


class DrugMovementListView(ListAPIView):
    queryset = DrugMovement.objects.all()
    serializer_class = DrugMovementSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('stock__institution',)
